const Hapi = require("@hapi/hapi");
const sqlite3 = require("sqlite3");
const db = new sqlite3.Database("cars.db");
const Path = require("path");
const inert = require("@hapi/inert");
const Joi = require("@hapi/joi");

const init = async () => {
  const server = Hapi.server({
    port: 3000,
    host: "localhost",
    routes: {
      cors: true,
      files: {
        relativeTo: Path.join(__dirname, "../react-crud"),
      },
    },
  });

  await server.register(inert);

  server.route({
    method: "GET",
    path: "/{param*}",
    handler: {
      directory: {
        path: ".",
        redirectToSlash: true,
        index: true,
      },
    },
  });
  server.route({
    method: "GET",
    path: "/cars",

    handler: async (request, h) => {
      try {
        const query = await new Promise((resolve, reject) => {
          db.all(
            "SELECT * FROM Cars order by id desc limit 100",
            (error, rows) => {
              if (error) {
                reject(error);
              } else {
                resolve(rows);
              }
            }
          );
        });
        if (query.length === 0) {
          return h.response({ message: "No cars found" }).code(404);
        }
        return h.response(query).code(200);
      } catch (error) {
        console.error(error);
        return h.response({ error: "Internal Server Error" }).code(500);
      }
    },
  });

  server.route({
    method: "POST",
    path: "/cars",
    options: {
      validate: {
        payload: Joi.object({
          name: Joi.string().min(1).max(20).required(),
          mark: Joi.string().min(1).max(20).required(),
          age: Joi.number().integer().required(),
          country: Joi.string().min(1).max(20).required(),
        }),
      },
    },

    handler: async (request, h) => {
      const { name, mark, age, country } = request.payload;
      const sql = `INSERT INTO Cars(Name, Mark, Age, Country) VALUES (?, ?, ?, ?)`;
      try {
        const result = await new Promise((resolve, reject) => {
          db.run(sql, [name, mark, age, country], function (error) {
            if (error) {
              reject(error);
            } else {
              resolve(this.lastID);
            }
          });
        });
        return h
          .response({ id: result, message: "Car added successfully" })
          .code(201);
      } catch (error) {
        console.error(error);
        return h.response({ error: "Internal Server Error" }).code(500);
      }
    },
  });

  server.route({
    method: "PUT",
    path: "/cars/{id}",
    handler: async (request, h) => {
      const { id } = request.params;
      const { name, mark, age, country } = request.payload;
      const sql = `UPDATE Cars SET Name = ?, Mark = ?, Age = ?, Country = ? WHERE ID = ?`;
      try {
        const result = await new Promise((resolve, reject) => {
          db.run(sql, [name, mark, age, country, id], function (error) {
            if (error) {
              reject(error);
            } else {
              resolve(this.changes);
            }
          });
        });
        if (result === 0) {
          return h.response({ message: "Car not found" }).code(404);
        }
        return h.response({ message: "Car updated successfully" }).code(200);
      } catch (error) {
        console.error(error);
        return h.response({ error: "Internal Server Error" }).code(500);
      }
    },
  });

  server.route({
    method: "DELETE",
    path: "/cars/{id}",
    options: {
      validate: {
        params: Joi.object({
          id: Joi.number().integer().positive().required(),
        }),
      },
    },
    handler: async (request, h) => {
      const { id } = request.params;
      const sql = `DELETE FROM Cars WHERE ID = ?`;
      try {
        const result = await new Promise((resolve, reject) => {
          db.run(sql, [id], function (error) {
            if (error) {
              reject(error);
            } else {
              resolve(this.changes);
            }
          });
        });
        if (result === 0) {
          return h.response({ message: "Car not found" }).code(404);
        }
        return h.response({ message: "Car deleted successfully" }).code(204);
      } catch (error) {
        console.error(error);
        return h.response({ error: "Internal Server Error" }).code(500);
      }
    },
  });

  await server.start();
  console.log("Server running on %s", server.info.uri);
};

process.on("unhandledRejection", (err) => {
  console.log(err);
  process.exit(1);
});

init();
