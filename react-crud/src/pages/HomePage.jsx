import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Card, Button, Image, Loader, Segment, Dimmer } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css';

const HomePage = () => {
  const [cars, setCars] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const getCars = async () => {
    try {
      const response = await axios.get('http://localhost:3000/cars');
      setCars(response.data);
      setIsLoading(false);
    } catch (error) {
      console.error('Failed to fetch cars:', error);
      setIsLoading(false);
    }
  };

  

  useEffect(() => {
    getCars();
  }, []);

  return (
    <div>
      <Segment style={{ padding: '2em 0em' }} vertical>
        <Button as={Link} to="/create" primary style={{ marginBottom: '2em' }}>
          Create a Car
        </Button>

        {isLoading ? (
          <Loader active inline='centered'>Loading Cars...</Loader>
        ) : (
          <Card.Group itemsPerRow={6}>
            {cars.map((car) => (
              <Card key={car.ID}>
                <Image src='\honda-cabrio.jpg' wrapped ui={false} />
                <Card.Content>
                  
                  <Card.Meta>{car.Mark}</Card.Meta>
                  <Card.Description>
                    Age: {car.Age}<br/>
                    Country: {car.Country}<br/>
                    Id: {car.ID}
                  </Card.Description>
                </Card.Content>
                <Card.Content extra>
                  <div className='ui two buttons'>
                    <Button primary basic color='green' as={Link} to={`/edit/${car.ID}`}>
                      Edit
                    </Button>
                    <Button secondary basic color='red' as={Link} to ={`/delete/${car.ID}`}>
                      Delete
                    </Button>
                  </div> 
                </Card.Content>
              </Card>
            ))}
          </Card.Group>
        )}
      </Segment>
    </div>
  );
};

export default HomePage;