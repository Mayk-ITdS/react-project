import React from 'react';
import './App.css';
import { Link, Routes, Route } from 'react-router-dom';
import HomePage from './pages/HomePage';
import CreatePage from './pages/CreatePage';
import EditPage from './pages/EditPage';
import DeletePage from './pages/DeletePage';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import 'semantic-ui-css/semantic.min.css';
const App = () => {
  return (
    <div className="App">
      <nav className="navbar">
      <Link to="/"><h2>React-Project</h2></Link>
        <div className="container">
      
        </div>
      </nav>
      <div className='container'>
        <Routes>
          <Route index element={<HomePage/>}></Route>
          <Route path="/create" element={<CreatePage/>}></Route>
          <Route path="/edit/:id" element={<EditPage/>}></Route>
          <Route path="/delete/:id" element={<DeletePage/>}></Route> 
        </Routes>
      </div>
      <ToastContainer/>
    </div>
  );
};

export default App;