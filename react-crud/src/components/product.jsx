import { Link } from "react-router-dom";

const Product = ({car}) => {
    let source = () => { 
    if (car.Name = 'Sedan'){
        return '/honda-sedan.jpg'
    } 
    if (car.Name = 'Cabrio')
    {
        return '/honda-cabrio.jpg'    
    }
        
    
     
        
     
        
};
    return (
        <div className="Product">
            <img src={source()} className="w-full h-28 object-cover"/>
            <div className="px-4 pt-2 pb-4">
                <div className="text-sm">ID: {car.ID}</div>
                <h2 className="text font-semibold">{car.Name}</h2>
                <div className="text-sm">Age: {car.Age} years</div>
                <div className="text-sm">Country: {car.Country}</div>
                <Link to={`/edit/${car.ID}`} className="edit-button">Edit</Link>
                <Link to={`/delete/${car.ID}`} className="delete-button">Delete</Link> 
            </div>
            <div className="ProductActions">
                
                
            
            </div>
        </div>
    )
}

export default Product;